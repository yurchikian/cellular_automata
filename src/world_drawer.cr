require "stumpy_png"
include StumpyPNG

module WorldDrawer
  extend self

  def draw(world : World)
    canvas = Canvas.new(world.width.to_i, world.height.to_i)

    world.width.times do |x|
      world.height.times do |y|
        world_energy = world.solar_power_at(x, y)
        color = RGBA.from_rgb_n(64 * world_energy, 12, 12, 8)

        cell = world[x, y]
        
        if cell.is_a? Cell
          cell_colors = [
            cell.genome.hash % 128,
            (cell.genome.hash >> 4) % 128,
            (cell.genome.hash >> 2) % 128,
          ]

          cell_r, cell_g, cell_b = cell_colors.map { |c| c * cell.energy / (cell_colors.max + 1) }

          color = RGBA.from_rgb_n(128 + cell_r, 128 + cell_g, 128 + cell_b, 8)
        end

        canvas[x, y] = color
      end
    end

    Dir.mkdir_p("./output")
    StumpyPNG.write(canvas, "./output/w_at_%08d.png" % world.current_step)
  end
end