struct Genome
  property :genes, :hash

  def initialize
    @genes = Array(UInt8).new(1 + UInt8::MAX, 0)
    @hash = 0_u32
  end

  def initialize(@genes, @hash)
  end

  def mutate(amount = 1_u32)
    count = @genes.size

    amount.times do
      gene_to_mutate = RANDOM.rand(count)
      new_value = RANDOM.rand(count)

      @genes[gene_to_mutate] &+= new_value
    end
    
    @hash = RANDOM.next_u if amount > 0
  end

  def mutated(amount = 1)
    genome = Genome.new(@genes.dup, @hash)
    genome.mutate(amount)
    genome
  end

  def [](position : UInt8)
    @genes[position]
  end
end