class Cell
  property :state
  property :genome
  property :steps_left
  property :energy, :energy_delta
  property :x, :y, :world
  property :is_new

  def initialize(
    @genome : Genome,
    @world : World,
    @x : UInt32,
    @y : UInt32,
    @energy : Int8 = CONFIG.cell[:initial_energy],
    @direction : Int8 = (RANDOM.next_int % 8).to_i8,
    @state = 0_u8
  )

    @energy_delta = 0_i8
    @steps_left = 0_u8
    @is_new = true
    @alive = true
  end

  def update_properties
    @energy &+= @energy_delta
    @energy_delta = 0_i8

    @is_new = false if @is_new

    if @energy < 0
      die!
    end
  end

  def step
    case @genome[@state]
    when 0
      photosynthesis
      abort
    when 1
      move
      abort
    when 2
      check_around
    when 3
      divide
      abort
    when 4
      check_energy
    when 5
      give_energy
    when 6
      eat
      abort
    when 7
      preserve_energy
      abort
    when 8
      check_solar
    when 9
      check_greener_grass
    when 10
      turn
    when 11
      share_energy_with_friends
      abort
    when 12
      check_if_stronger
    when 13
      die!
      abort
    else
      no_operation
    end

    @energy_delta &-= CONFIG.cell[:energy_decay]

    if @steps_left < 1
      check_for_division
    end
    
    keep_going
  end

  ### Actions
  private def photosynthesis
    solar_power = @world.solar_power_at(@x, @y)
    
    @energy_delta += (solar_power * CONFIG.cell[:solar_multiplier]).to_i
    increment_state
  end

  private def turn
    direction = next_gene % 2
    sharpness = next_gene % 3 + 1

    if direction == 0
      @direction &+= sharpness
    else
      @direction &-= sharpness
    end

    @direction = @direction % 8

    increment_state 3
  end
  
  private def move
    nx, ny = position_at_direction(@direction)
    @world.move(@x, @y, nx, ny)
    @energy_delta &-= CONFIG.cell[:movement_cost]
    
    increment_state
  end
  
  private def check_around
    nx, ny = position_at_direction(@direction)
    cell = @world[nx, ny]

    case cell
    when Cell
      if cell.genome.hash == @genome.hash
        increment_state 2
      else
        increment_state 3
      end
    else
      increment_state
    end
  end

  private def divide(fraction = 4)
    increment_state

    nx, ny = position_at_direction(@direction)
    target_cell = @world[nx, ny]

    energy_fraction = @energy / fraction
    @energy_delta &-= energy_fraction.to_i
    
    if target_cell.is_a? Cell
      if CONFIG.cell[:die_on_division_to_foe] && target_cell.genome.hash != @genome.hash
        die!
      end

      increment_state

      return
    end

    nx, ny = @world.tile(nx, ny)
    
    mutation = (RANDOM.rand < CONFIG.cell[:mutation_chance]) ? 1 : 0
    new_genome = @genome.mutated(mutation)
    child = Cell.new(new_genome, @world, nx, ny, energy_fraction.to_i8, @direction)

    @world.add_cell(child)
  end

  private def check_energy
    increment_state(@energy / (128 / CONFIG.cell[:check_energy_gradations]) + 1)
  end

  private def give_energy
    amount = (next_gene % CONFIG.cell[:give_energy_gradations] + 1) * CONFIG.cell[:give_energy_multiplier]

    increment_state 2

    gx, gy = position_at_direction(@direction)
    give_to = @world[gx, gy]

    @energy_delta &-= amount

    return unless give_to.is_a? Cell

    give_to.energy_delta &+= amount
  end

  private def eat
    nx, ny = position_at_direction(@direction)
    
    increment_state

    @energy_delta &-= CONFIG.cell[:eat_cost]

    target_cell = @world[nx, ny]
    return unless target_cell.is_a? Cell

    unless CONFIG.cell[:can_eat_friends]
      return if target_cell.genome.hash == @genome.hash
    end

    unless CONFIG.cell[:can_eat_stronger]
      return if target_cell.energy > @energy
    end

    increment_state

    @energy_delta &+= target_cell.energy
    target_cell.die!
  end

  private def preserve_energy
    @energy_delta &+= @steps_left
    increment_state
  end

  private def check_solar
    increment_state (@world.solar_power_at(@x, @y) * CONFIG.cell[:check_solar_gradations] + 1).to_u8
  end

  private def check_greener_grass
    nx, ny = position_at_direction(@direction)

    if @world.solar_power_at(nx, ny) > @world.solar_power_at(@x, @y)
      increment_state 1
    else
      increment_state 2
    end
  end

  private def share_energy_with_friends
    amount_to_share = (next_gene % CONFIG.cell[:share_energy_gradations] + 1) * CONFIG.cell[:share_energy_multiplier]

    surrounding_cells.each do |cell|
      next unless cell.is_a? Cell
      next unless cell.genome.hash == @genome.hash

      @energy_delta &-= amount_to_share
      cell.energy_delta &+= amount_to_share
    end

    increment_state 2
  end

  private def check_if_stronger
    nx, ny = position_at_direction(@direction)

    target_cell = @world[nx, ny]

    return unless target_cell.is_a? Cell

    if target_cell.energy > @energy
      increment_state 1
    else
      increment_state 2
    end
  end
  
  private def no_operation
    increment_state @genome[@state]
  end

  ### Post-step operations
  private def check_for_division
    return unless @energy > CONFIG.cell[:min_energy_to_force_division]
    divide CONFIG.cell[:force_division_energy_fraction]
    abort
  end

  ### Available steps manupulation
  private def abort
    @steps_left = 0
  end

  private def keep_going
    @steps_left &-= 1
  end
  
  ### Helpers
  def alive?
    @alive
  end

  def die!
    @alive = false
  end

  private def surrounding_cells
    (0..7).map do |direction|
      x, y = position_at_direction(direction)
      @world[x, y]
    end
  end

  private def increment_state(steps = 1_u8)
    @state &+= steps.to_u8
  end

  private def next_gene(n : UInt8 = 1)
    next_state = @state &+ n
    @genome[next_state]
  end

  DIRECTIONS = [
    [-1, -1],
    [ 0, -1],
    [ 1, -1],
    [-1,  0],
    [ 1,  0],
    [-1,  1],
    [ 0,  1],
    [ 1,  1]
  ]

  private def position_at_direction(direction)
    dx, dy = DIRECTIONS[direction]
    [@x.to_i &+ dx, @y.to_i &+ dy]
  end

  def on_death
    # p "Cell at #{[x, y]} died"
  end
end
