class World
  property :cells
  property :width, :height
  property :current_step

  def initialize(@width = 128_u32, @height = 128_u32)
    @cells = Array(Cell | Nil).new(@width * @height) { nil }
    @current_step = 0_u32

    @depth_power = Array(Float64).new(@height) { |y| depth_power_at(y) }
  end

  def step
    @current_step += 1

    @cells.each { |cell| step(cell) }
    @cells.each { |cell| update(cell) }
  end

  def [](x, y)
    @cells[offset(x, y)]
  end

  def []=(x, y, value)
    @cells[offset(x, y)] = value
  end

  def offset(x, y)
    x, y = tile(x, y)
    x * @height + y
  end

  def add_cell(cell : Cell)
    self[cell.x, cell.y] = cell
  end

  def update(cell)
    return unless cell.is_a? Cell

    cell.update_properties

    unless cell.alive?
      cell.on_death
      self[cell.x, cell.y] = nil
    end
  end

  def step(cell)
    return unless cell.is_a? Cell
    return if cell.is_new

    cell.steps_left = CONFIG.substeps
    
    while cell.steps_left > 0
      cell.step
    end
  end

  def move(x, y, new_x, new_y)
    the_cell = self[x, y]

    return if self[new_x, new_y].is_a? Cell
    return if the_cell.is_a? Nil

    new_x, new_y = tile(new_x, new_y)

    the_cell.x = new_x
    the_cell.y = new_y

    self[new_x, new_y] = self[x, y]
    self[x, y] = nil
  end

  def tile(x, y)
    [(x % @width).to_u, (y % @height).to_u]
  end

  def draw
    WorldDrawer.draw(self)
  end

  def depth_power_at(y)
    depth = y.to_f / @height.to_f
    
    power = Math.cos(2 * (depth - 0.5) * Math::PI) / 2 + 0.5
    power = power ** CONFIG.world[:solar_power]

    total_minimum = CONFIG.world[:total_minimum]
    power / ( 1.0 / (1.0 - total_minimum)) + total_minimum
  end

  def solar_power_at(x, y)
    x, y = tile(x, y)
    @depth_power[y]
  end
end
