require "./config.cr"
require "./entities/*"
require "./world_drawer.cr"

require "benchmark"

CONFIG = Config.configure
RANDOM = Random.new(CONFIG.random_seed)

module CellularAutomata

  world_width = CONFIG.world[:width]
  world_height = CONFIG.world[:height]

  world = World.new(world_width, world_height)

  CONFIG.starting_cells.times do
    x, y = [RANDOM.next_int, RANDOM.next_int]

    genome = Genome.new
    genome.mutate(CONFIG.initial_mutation)

    x, y = world.tile(x, y)
    world.add_cell(Cell.new(genome, world, x, y))
  end

  iterations = CONFIG.iterations

  digits = Math.log10(iterations).to_i + 1

  average = Time::Span.new

  (iterations + 1).times do |i|
    
    if i % CONFIG.draw_each == 0
      world.draw

      step_time = Time.measure do
        world.step
      end

      average = average * 11 + step_time
      average /= 12

      steps_left = iterations - i
      time_left = average * steps_left

      output_params = [i, iterations, time_left.hours, time_left.minutes, time_left.seconds + 1]

      print "World iteration %0#{digits}d/%0#{digits}d (%02d:%02d:%02d left) #{" " * 10}\r" % output_params
      STDOUT.flush
    else
      world.step
    end
  end

end
