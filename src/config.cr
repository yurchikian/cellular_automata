require "totem"

struct Config
  include Totem::ConfigBuilder

  property random_seed : UInt64
  property world : NamedTuple(
    height: UInt32,
    width: UInt32,
    solar_power: Float64,
    total_minimum: Float64
  )

  property initial_mutation : UInt32
  property starting_cells : UInt32

  property iterations : UInt32
  property draw_each : UInt32

  property cell : NamedTuple(
    initial_energy: Int8,
    energy_decay: Int8,
    solar_multiplier: Float64,
    movement_cost: UInt8,
    check_energy_gradations: UInt8,
    give_energy_gradations: UInt8,
    give_energy_multiplier: UInt8,
    eat_cost: UInt8,
    can_eat_friends: Bool,
    can_eat_stronger: Bool,
    check_solar_gradations: UInt8,
    share_energy_gradations: UInt8,
    share_energy_multiplier: UInt8,
    min_energy_to_force_division: Int8,
    force_division_energy_fraction: UInt8,
    mutation_chance: Float64,
    die_on_division_to_foe: Bool
  )

  property substeps : UInt8

  build do
    config_type "yaml"
    config_paths [".", "./config/"]
  end
end